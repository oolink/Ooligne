package com.oolink.ooligne;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Authentification des administrateur de l'application.
 */

public class SigninActivity extends BaseActivity {

    @BindView(R.id.signin_email)
    EditText email;

    @BindView(R.id.signin_password)
    EditText password;

    //Initialize an instance of Cloud Firestore:
    FirebaseFirestore db;

    private FirebaseAuth mAuth;


    @Override
    protected void initView() {
        setContentView(R.layout.activity_signin);
    }

    @Override
    protected void onCreated(Bundle savedInstanceState) {
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.signin)
    public void login() {
        if (Texts.isEmpty(email.getText()) || Texts.isEmpty(password.getText())) {
            Toast.makeText(SigninActivity.this,
                    "Please Fill all the required fields, thanks",
                    Toast.LENGTH_LONG).show();
            return;
        }
        mAuth.signInWithEmailAndPassword(email.getText().toString().trim(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SigninActivity", "signInWithEmail:success");
                            Toast.makeText(SigninActivity.this, "Authentication success.",
                                    Toast.LENGTH_LONG).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            System.out.println(user.getEmail());
                            System.out.println(user.getDisplayName());
                            System.out.println("---------------------");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("SigninActivity", "signInWithEmail:failure", task.getException());
                            Toast.makeText(SigninActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    @OnClick(R.id.signin_reset)
    public void resetPassword(){
        if (Texts.isEmpty(email.getText())) {
            Toast.makeText(SigninActivity.this,
                    "Please Fill your mail adress, thanks",
                    Toast.LENGTH_LONG).show();
            return;
        }
        mAuth.sendPasswordResetEmail(email.getText().toString())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(SigninActivity.this, "Email sent.",
                                Toast.LENGTH_SHORT).show();
                        Log.d("SigninActivity", "Email sent.");
                    }
                });
    }
}
