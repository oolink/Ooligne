package com.oolink.ooligne;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.SQLOutput;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import butterknife.BindView;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

import static cz.msebera.android.httpclient.HttpVersion.HTTP;

/**
 * Enregistrement d'un nouveau candidat .
 */

public class SignupActivity extends BaseActivity {

    @BindView(R.id.signup_firstName)
    EditText firstName;

    @BindView(R.id.signup_lastName)
    EditText lastName;

    @BindView(R.id.signup_adresse)
    EditText adresse;

    @BindView(R.id.signup_cp)
    EditText cp;

    @BindView(R.id.signup_ville)
    EditText ville;

    @BindView(R.id.signup_phone)
    EditText phone;

    @BindView(R.id.signup_email)
    EditText email;

    @BindView(R.id.signup_occuped_post)
    EditText occupedPost;

    @BindView(R.id.signup_searched_post)
    EditText searchedPost;

    @BindView(R.id.signup_find_oolink)
    EditText findOolink;

    @BindView(R.id.signup_passion)
    EditText passion;

    @BindView(R.id.signup_save)
    Button save;

    @BindView(R.id.signup_update)
    Button update;

    @BindView(R.id.indeterminateBar)
    ProgressBar progressBar;

    @BindView(R.id.home_constraint)
    ConstraintLayout home_constraint;

    //Initialize an instance of Cloud Firestore:
    FirebaseFirestore db;

    private FirebaseAnalytics mFirebaseAnalytics;

    StringEntity entity;

    // url to create new user
    private static String url_create_user = "http://www.oolinkeur.com/android_php/create_user.php";
    // url to create new user
    private static String url_update_user = "http://www.oolinkeur.com/android_php/update_user.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";


    int Status;
    String message;
    JSONObject jsonObj;
    User user;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_signup);
    }

    @Override
    protected void onCreated(Bundle savedInstanceState) {

        update.setVisibility(View.GONE);
        save.setVisibility(View.VISIBLE);

        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        email.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verfication_email(email.getText().toString().trim());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.signup_save)
    public void saveUser() {
        if (Texts.isEmpty(firstName.getText()) || Texts.isEmpty(lastName.getText()) ||
                Texts.isEmpty(phone.getText()) || Texts.isEmpty(adresse.getText()) ||
                Texts.isEmpty(cp.getText()) || Texts.isEmpty(ville.getText())
                || Texts.isEmpty(email.getText())) {
            Toast.makeText(SignupActivity.this,
                    "Please Fill all the required fields, thanks",
                    Toast.LENGTH_LONG).show();
            return;
        }

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        progressBar.setVisibility(View.VISIBLE);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("firstName", firstName.getText().toString());
        params.put("lastName", lastName.getText().toString());
        params.put("email", email.getText().toString());
        params.put("adresse", adresse.getText().toString());
        params.put("cp", cp.getText().toString());
        params.put("ville", ville.getText().toString());
        params.put("phone", phone.getText().toString());

        params.put("occuped_post", occupedPost.getText().toString());
        params.put("searched_post", searchedPost.getText().toString());
        params.put("find_oolink", findOolink.getText().toString());
        params.put("passion", passion.getText().toString());

        try {
            entity = new StringEntity(params.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (!isConnected){
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("user_params", params.toString());
            editor.apply();
            Toast.makeText(this,"user Successfuly added in Offline mode",Toast.LENGTH_LONG).show();
            startActivity(new Intent(SignupActivity.this,MainActivity.class));
        }

        System.out.println(params);
        client.post(SignupActivity.this,
                url_create_user, entity,
                "application/x-www-form-urlencoded",
                new TextHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String res) {
                        //* Get value from JSON *//*
                        try {
                            System.out.println(res);
                            JSONObject jsonObj = new JSONObject(res);
                            Status = Integer.valueOf(jsonObj.getString(TAG_SUCCESS));
                            message = jsonObj.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                        //* Test for status code *//*
                        switch (Status) {

                            case 200: //* Valid email and valid insert in DataBase *//*
                                updateUI();
                                break;

                            case 201: //* AlerDialog when click on Exit *//*
                                startActivity(new Intent(SignupActivity.this,MainActivity.class));
                                break;

                            case 401: //* Existing email *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 402: //* Existing Email and deleted *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 404: //* Erreur d'inscription_fragment *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 500: //* Existing Email and deleted *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SignupActivity.this, "Failure to add collab to database", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /* Insert Data In DataBase */
    public void Verfication_email(String mail) {
        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("Email", mail);

        try {
            entity = new StringEntity(params.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        client.post(SignupActivity.this,
                "http://www.oolinkeur.com/android_php/get_user_by_email.php",
                params,
                new TextHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String res) {
                    /* Get value from JSON */
                        try {
                            System.out.println("----------------");
                            System.out.println(res);
                            jsonObj = new JSONObject(res);
                            Status = Integer.valueOf(jsonObj.getString("success"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                    /* Test for status code */
                        switch (Status) {

                            case 1: /* Existing email */

                                update.setVisibility(View.VISIBLE);
                                save.setVisibility(View.GONE);
                                try {
                                    user = new User();
                                    JSONArray jsonObject = jsonObj.getJSONArray("user");

                                    user.setId(jsonObject.getJSONObject(0).getInt("id"));
                                    firstName.setText(jsonObject.getJSONObject(0).getString("firstName"));
                                    user.setFirstName(jsonObject.getJSONObject(0).getString("firstName"));
                                    lastName.setText(jsonObject.getJSONObject(0).getString("lastName"));
                                    user.setLastName(jsonObject.getJSONObject(0).getString("lastName"));
                                    phone.setText(jsonObject.getJSONObject(0).getString("phone"));
                                    user.setPhone(jsonObject.getJSONObject(0).getInt("phone"));

                                    adresse.setText(jsonObject.getJSONObject(0).getString("adresse"));
                                    user.setAdresse(jsonObject.getJSONObject(0).getString("adresse"));
                                    cp.setText(jsonObject.getJSONObject(0).getString("cp"));
                                    user.setCp(jsonObject.getJSONObject(0).getString("cp"));
                                    ville.setText(jsonObject.getJSONObject(0).getString("ville"));
                                    user.setVille(jsonObject.getJSONObject(0).getString("ville"));

                                    occupedPost.setText(jsonObject.getJSONObject(0).getString("occuped_post"));
                                    user.setOccupedPost(jsonObject.getJSONObject(0).getString("occuped_post"));
                                    searchedPost.setText(jsonObject.getJSONObject(0).getString("searched_post"));
                                    user.setSearchedPost(jsonObject.getJSONObject(0).getString("searched_post"));
                                    findOolink.setText(jsonObject.getJSONObject(0).getString("find_oolink"));
                                    user.setFindOolink(jsonObject.getJSONObject(0).getString("find_oolink"));
                                    passion.setText(jsonObject.getJSONObject(0).getString("passion"));
                                    user.setPassion(jsonObject.getJSONObject(0).getString("passion"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                hideSoftKeyboard(SignupActivity.this);
                                break;
                                case 0:
                                    update.setVisibility(View.GONE);
                                    save.setVisibility(View.VISIBLE);

                                    firstName.getText().clear();
                                    lastName.getText().clear();
                                    adresse.getText().clear();
                                    cp.getText().clear();
                                    ville.getText().clear();
                                    phone.getText().clear();

                                    occupedPost.getText().clear();
                                    searchedPost.getText().clear();
                                    findOolink.getText().clear();
                                    passion.getText().clear();
                                break;

                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SignupActivity.this, "Failure to add collab to database", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @OnClick(R.id.signup_update)
    public void updateUser() {
        if (Texts.isEmpty(firstName.getText()) || Texts.isEmpty(lastName.getText()) ||
                Texts.isEmpty(phone.getText()) || Texts.isEmpty(adresse.getText()) ||
                Texts.isEmpty(cp.getText()) || Texts.isEmpty(ville.getText())
                || Texts.isEmpty(email.getText())) {
            Toast.makeText(SignupActivity.this,
                    "Please Fill all the required fields, thanks",
                    Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("id",user.getId());
        params.put("firstName", firstName.getText().toString());
        params.put("lastName", lastName.getText().toString());
        params.put("email", email.getText().toString());
        params.put("adresse", adresse.getText().toString());
        params.put("cp", cp.getText().toString());
        params.put("ville", ville.getText().toString());
        params.put("phone", phone.getText().toString());

        params.put("occuped_post", occupedPost.getText().toString());
        params.put("searched_post", searchedPost.getText().toString());
        params.put("find_oolink", findOolink.getText().toString());
        params.put("passion", passion.getText().toString());

        try {
            entity = new StringEntity(params.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        client.post(SignupActivity.this,
                url_update_user, entity,
                "application/x-www-form-urlencoded",
                new TextHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String res) {
                        //* Get value from JSON *//*
                        try {
                            JSONObject jsonObj = new JSONObject(res);
                            Status = Integer.valueOf(jsonObj.getString(TAG_SUCCESS));
                            message = jsonObj.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                        //* Test for status code *//*
                        switch (Status) {

                            case 200: //* Valid email and valid insert in DataBase *//*
                                Snackbar snackbar = Snackbar.make(home_constraint, message, Snackbar.LENGTH_LONG);
                                snackbar.show();
                                startActivity(new Intent(SignupActivity.this,MainActivity.class));
                                break;

                            case 201: //* AlerDialog when click on Exit *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 401: //* Existing email *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 402: //* Existing Email and deleted *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 404: //* Erreur d'inscription_fragment *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;

                            case 500: //* Existing Email and deleted *//*
                                Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SignupActivity.this, "Failure to add collab to database", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void updateUI(){
        mFirebaseAnalytics.setUserProperty("passion", passion.getText().toString());
        Bundle para = new Bundle();
        para.putString("passion", passion.getText().toString());
        mFirebaseAnalytics.logEvent("share_image", para);
        Snackbar snackbar = Snackbar.make(home_constraint, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        startActivity(new Intent(SignupActivity.this,MainActivity.class));
    }
}
