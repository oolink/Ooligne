package com.oolink.ooligne;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * <p>
 * Couche parente des activités simples.<br/>
 * Cette couche parente supporte les fragments.
 * </p>
 */
public abstract class BaseActivity extends AppCompatActivity {

    private int Status;
    private ProgressDialog pDialog;

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();
        ButterKnife.bind(this);
        onCreated(savedInstanceState);

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            String params = sharedPref.getString("user_params",null);
            if(params != null){

                pDialog = new ProgressDialog(BaseActivity.this);
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();

                AsyncHttpClient client = new AsyncHttpClient();
                StringEntity entity = null;

                try {
                    entity = new StringEntity(params);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                client.post(this,
                        "http://www.oolinkeur.com/android_php/create_user.php", entity,
                        "application/x-www-form-urlencoded",
                        new TextHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String res) {
                                pDialog.dismiss();
                                //* Get value from JSON *//*
                                try {
                                    JSONObject jsonObj = new JSONObject(res);
                                    Status = Integer.valueOf(jsonObj.getString("success"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                //* Test for status code *//*
                                switch (Status) {

                                    case 200: //* Valid email and valid insert in DataBase *//*
                                        editor.clear();
                                        editor.apply();
                                        Toast.makeText(BaseActivity.this, "collab added to database", Toast.LENGTH_LONG).show();
                                        break;

                                    case 201: //* AlerDialog when click on Exit *//*
                                        System.out.println("---------201---------");
                                        break;

                                    case 401: //* Existing email *//*
                                        System.out.println("---------401---------");
                                        break;

                                    case 402: //* Existing Email and deleted *//*
                                        System.out.println("-----------402--------");
                                        break;

                                    case 404: //* Erreur d'inscription_fragment *//*
                                        System.out.println("--------404-----------");
                                        break;

                                    case 500: //* Existing Email and deleted *//*
                                        System.out.println("500 - missing params");
                                        break;
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                                pDialog.dismiss();
                            }
                        });
            }
        }
    }

    /**
     * Initialise la vue de l'activité.
     */
    protected abstract void initView();

    /**
     * <p>
     * Callback exécuté une fois l'activité créée, à la fin de l'exécution de la méthode
     * {@link AppCompatActivity#onCreate(Bundle)}.
     * </p>
     * A ce stade, les vues et les composants injectés sont utilisables.
     *
     * @param savedInstanceState Voir la javadoc de {@link android.app.Activity#onCreate(Bundle)}.
     */
    protected abstract void onCreated(Bundle savedInstanceState);

    @Override
    public void onBackPressed() {

    }

    @Override
    protected final void onStart() {
        super.onStart();
        onStarted();
    }

    /**
     * S'appelle après avoir appeler le {@code onStart()} du parent et avant le {@code onResume()}.
     */
    protected void onStarted() {
        // L'implémentation par défaut ne fait rien.
    }

    @Override
    protected final void onStop() {
        onStopped();
        super.onStop();
    }

    /**
     * S'appelle après {@code onPause()} et avant le {@code onStop()} du Parent.
     */
    protected void onStopped() {
        // L'implémentation par défaut ne fait rien.
    }

    /**
     * Implementation par défaut de la méthode que l'on appelle lors de la detection
     * de connexion à internet.
     *
     * @param hasNet True si on a le net.
     */
    protected void onNetworkChanged(boolean hasNet) {
        // L'implémentation par défaut ne fait rien.
    }
}
