package com.oolink.ooligne;

/**
 * Elle représente l'entité du candidat
 */

public class User {

    private int id;
    private String firstName;
    private String lastName;
    private String adresse;
    private String cp;
    private String ville;
    private String email;
    private int phone;

    private String occupedPost;
    private String searchedPost;
    private String findOolink;
    private String passion;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOccupedPost() {
        return occupedPost;
    }

    public void setOccupedPost(String occupedPost) {
        this.occupedPost = occupedPost;
    }

    public String getSearchedPost() {
        return searchedPost;
    }

    public void setSearchedPost(String searchedPost) {
        this.searchedPost = searchedPost;
    }

    public String getFindOolink() {
        return findOolink;
    }

    public void setFindOolink(String findOolink) {
        this.findOolink = findOolink;
    }

    public String getPassion() {
        return passion;
    }

    public void setPassion(String passion) {
        this.passion = passion;
    }
}
