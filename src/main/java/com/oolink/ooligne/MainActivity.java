package com.oolink.ooligne;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.oolink.ooligne.splashscreen.ui.SplashScreenActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.home_constraint)
    ConstraintLayout constraintLayout;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void onCreated(Bundle savedInstanceState) {

    }

    @OnClick(R.id.connexion)
    protected void connexion(){
        /* Create an Intent that will start the Sign in Activity. */
        Intent mainIntent = new Intent(MainActivity.this,SigninActivity.class);
        startActivity(mainIntent);
    }

    @OnClick(R.id.inscription)
    protected void inscription(){
         /* Create an Intent that will start the Sign up Activity. */
        Intent mainIntent = new Intent(MainActivity.this,SignupActivity.class);
        startActivity(mainIntent);
    }

    @OnClick(R.id.oolinkweb)
    protected void OolinkWeb(){
        /* Create an Intent that will start the Oolink web Activity. */
        Intent mainIntent = new Intent(MainActivity.this,OolinkWebActivity.class);
        startActivity(mainIntent);
    }
}
