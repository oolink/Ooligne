package com.oolink.ooligne;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;

import butterknife.BindView;

/**
 * activity responsable du charment du site web Oolink
 */

public class OolinkWebActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webView;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_oolink_web);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreated(Bundle savedInstanceState) {
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        webView.loadUrl("http://oolink.fr/");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
