package com.oolink.ooligne.splashscreen.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.widget.ImageView;


import com.google.firebase.crash.FirebaseCrash;
import com.oolink.ooligne.BaseActivity;
import com.oolink.ooligne.MainActivity;
import com.oolink.ooligne.R;

import butterknife.BindView;


/**
 * Lance le splash screen et extrait les données en arrière-plan
 */
public class SplashScreenActivity extends BaseActivity {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @BindView(R.id.splash_screen_logo)
    ImageView logo;


    @Override
    protected void initView() {
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onCreated(Bundle savedInstanceState) {
    /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Menu-Activity. */
            Intent mainIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(SplashScreenActivity.this,
                            logo,
                            ViewCompat.getTransitionName(logo));
            FirebaseCrash.report(new Exception("My first Android non-fatal error"));
            startActivity(mainIntent, options.toBundle());
            //finish();
        }, SPLASH_DISPLAY_LENGTH);
    }
}
