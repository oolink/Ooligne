package com.oolink.ooligne;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Classe utilitaire sur les textes.
 */
public final class Texts {

    /**
     * Map associant un ensemble de caractères accentués à son caractère normalisé correspondant :
     * <pre>
     *     [àáâãäå] -> a
     *     [ÀÁÂÃÄÅ] -> A
     *     [èééêë] -> e
     *     [ÈÉÉÊË] -> E
     *     ...
     * </pre>
     */
    private static final Map<String, Character> REPLACE = new HashMap<>();

    static {
        for (final String set : Arrays.asList(
                "aàáâãäå",
                "cç",
                "eèééêë",
                "iìíîï",
                "oóòôõö",
                "uùúûü"
        )) {
            final Character firstChar = set.charAt(0);
            final String substring = set.substring(1);
            REPLACE.put('[' + substring + ']', firstChar);
            REPLACE.put('[' + toUpperCase(substring) + ']', Character.toUpperCase(firstChar));
        }
    }

    private Texts() {
        // Constructeur privé de classe utilitaire.
    }

    /**
     * Indique si le texte fourni est {@code null} ou de taille égale à 0.
     * <pre>
     *     isEmpty(null) -> true
     *     isEmpty("") -> true
     *     isEmpty(" ") -> false
     *     isEmpty("value") -> false
     *     isEmpty(" value ") -> false
     * </pre>
     *
     * @param text Le texte à vérifier.
     * @return {@code true} si le texte est {@code null} ou de taille 0, {@code false} sinon.
     */
    public static boolean isEmpty(final CharSequence text) {
        return text == null || text.length() == 0;
    }

    /**
     * Indique si le texte fourni est {@code null} ou ne contient que des espaces.
     * <pre>
     *     isBlank(null) -> true
     *     isBlank("") -> true
     *     isBlank(" ") -> true
     *     isBlank("value") -> false
     *     isBlank(" value ") -> false
     * </pre>
     *
     * @param text Le texte à vérifier.
     * @return {@code true} si le texte est {@code null} ou ne contient que des espaces, {@code false} sinon.
     */
    public static boolean isBlank(final CharSequence text) {
        int strLen;
        if (text == null || (strLen = text.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(text.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Indique si le texte fourni n'est ni vide, ni {@code null}, ni ne contient que des espaces.
     * <pre>
     *     isNotBlank(null) -> false
     *     isNotBlank("") -> false
     *     isNotBlank(" ") -> false
     *     isNotBlank("value") -> true
     *     isNotBlank(" value ") -> true
     * </pre>
     *
     * @param text Le texte à vérifier.
     * @return {@code true} si le texte fourni n'est ni vide, ni {@code null}, ni ne contient que des espaces.
     * Sinon retourne {@code false}.
     */
    public static boolean isNotBlank(final CharSequence text) {
        return !isBlank(text);
    }

    /**
     * Retourne le texte fourni avec le premier caractère en majuscule et le reste en minuscule.
     * <pre>
     *     capitalize(null) -> null
     *     capitalize("") -> ""
     *     capitalize("value") -> "Value"
     *     capitalize("vALUE") -> "Value"
     *     capitalize("VALUE") -> "Value"
     *     capitalize("ValuE") -> "Value"
     * </pre>
     *
     * @param text Le texte à <em>capitaliser</em>.
     * @return Le texte <em>capitalisé</em>, ou {@code null}.
     */
    public static String capitalize(final String text) {
        if (isBlank(text)) {
            return text;
        } else if (text.length() == 1) {
            return String.valueOf(Character.toUpperCase(text.charAt(0)));
        } else {
            return Character.toUpperCase(text.charAt(0)) + toLowerCase(text.substring(1));
        }
    }

    /**
     * Retourne le texte fourni en minuscule.
     * <pre>
     *     toLowerCase(null) -> null
     *     toLowerCase("") -> ""
     *     toLowerCase("value") -> "value"
     *     toLowerCase("vALUE") -> "value"
     *     toLowerCase("VALUE") -> "value"
     *     toLowerCase("ValuE") -> "value"
     * </pre>
     *
     * @param text Le texte à mettre en minuscule.
     * @return Le texte en minuscule, ou {@code null}.
     */
    public static String toLowerCase(final String text) {
        return text == null ? null : text.toLowerCase(Locale.getDefault());
    }

    /**
     * Retourne le texte fourni en majuscule.
     * <pre>
     *     toUpperCase(null) -> null
     *     toUpperCase("") -> ""
     *     toUpperCase("value") -> "VALUE"
     *     toUpperCase("vALUE") -> "VALUE"
     *     toUpperCase("VALUE") -> "VALUE"
     *     toUpperCase("ValuE") -> "VALUE"
     * </pre>
     *
     * @param text Le texte à mettre en majuscule.
     * @return Le texte en majuscule, ou {@code null}.
     */
    public static String toUpperCase(final String text) {
        return text == null ? null : text.toUpperCase(Locale.getDefault());
    }

    /**
     * Retourne le texte fourni avec ses éventuels espaces de début et fin supprimés.
     * <pre>
     *     trim(null) -> null
     *     trim("") -> ""
     *     trim(" ") -> ""
     *     trim("  a  ") -> "a"
     *     trim("value") -> "value"
     *     trim(" value ") -> "value"
     * </pre>
     *
     * @param text Le texte à contrôler.
     * @return Le texte <em>trimé</em>, ou {@code null}.
     */
    public static String trim(final String text) {
        return text == null ? null : text.trim();
    }

    /**
     * Remplace dans le texte fourni les caractères accentués par leur caractère normalisé
     * correspondant.
     * <pre>
     *     normalize(null) -> null
     *     normalize("") -> ""
     *     normalize("Châtelet") -> "Chatelet"
     *     normalize("Fêté") -> "Fete"
     *     normalize("rome") -> "rome"
     * </pre>
     *
     * @param text Le texte à normaliser.
     * @return Le texte fourni en entrée dans lequel les caractères accentués ont été remplacés
     * par leur valeur normalisée, ou {@code null}.
     */
    public static String normalize(String text) {
        if (isBlank(text)) {
            return text;
        }
        for (final Map.Entry<String, Character> entry : REPLACE.entrySet()) {
            text = text.replaceAll(entry.getKey(), Character.toString(entry.getValue()));
        }
        return text;
    }
}
